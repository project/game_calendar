
Game Calendar

The Game Calendar module is a gaming utility that offers in-game calendars,
particularly suitable for RPG's, easily handling dates from medieval times to
the distant future. It will depend on both the Game Clock and Date API modules.

Roadmap:
--------
 * hook_game_clock('increment'): if the clock type is calendar, then
     retrieve the associated calendar and increment it accordingly.
