<?php

/**
 *  @file
 *  The administration page for the Game Calendar module.
 */

/**
 *  Callback for admin/settings/game_calendar.
 */
function game_calendar_list_page() {
  $output .= drupal_get_form('game_calendar_list_form');
  global $game_calendar_settings_page_standard_form;
  if ($game_calendar_settings_page_standard_form) {
    $output .= drupal_get_form('game_calendar_form');
    $output .= variable_get('game_calendar_settings_form_count', 50) ? theme('pager') : '';
  }
  return $output;
}

/**
 *  Listing of game calendars on top of the admin/settings/game_calendar page.
 */
function game_calendar_list_form($form_state) {
  if (isset($form_state['values']['selector']) && $form_state['values']['selector'] == 'delete') {
    foreach ($form_state['values']['calendars'] as $calendar => $selected) {
      if ($selected['selection']) {
        $items[$calendar] = $form_state['values']['game_calendars'][$calendar];
      }
    }
    return game_calendar_delete_form($form_state, $items);
  }
  else if (isset($form_state['values']['selector']) && $form_state['values']['selector'] == 'reset') {
    foreach ($form_state['values']['calendars'] as $calendar => $selected) {
      if ($selected['selection']) {
        $items[$calendar] = $form_state['values']['game_calendars'][$calendar];
      }
    }
    return game_calendar_reset_form($form_state, $items);
  }

  // We are printing the standard form. That means we get an add form and pager.
  global $game_calendar_settings_page_standard_form;
  $game_calendar_settings_page_standard_form = TRUE;

  $game_calendars = array();
  $header = array(
    theme('table_select_header_cell'),
    array('data' => t('Title'), 'field' => 'title', 'sort' => 'asc'),
    array('data' => t('Clock'), 'field' => 'cid'),
    array('data' => t('Status')),
    array('data' => t('Date'), 'field' => 'date'),
    array('data' => t('Rate'), 'field' => 'rate'),
    t('Ops'));
  $sql = "SELECT * FROM {game_calendars}";
  $sql .= tablesort_sql($header);
  if (variable_get('game_calendar_settings_form_count', 50)) {
    $results = pager_query($sql, variable_get('game_calendar_settings_form_count', 50));
  }
  else {
    $results = db_query($sql);
  }
  while ($state = db_fetch_object($results)) {
    $state->date = date_convert($state->date, DATE_ISO, DATE_OBJECT);
    $game_calendars[$state->name] = $state;
  }

  $form = array();
  $form['table_header'] = array(
    '#type' => 'value',
    '#value' => $header,
  );
  $options = array(
    'pause' => t('Pause game calendars'),
    'start' => t('Start game calendars'),
    'reset' => t('Reset game calendars'),
    'delete' => t('Delete game calendars'),
  );
  $form['selector'] = array(
    '#type' => 'select',
    '#title' => t('With selected game calendars'),
    '#options' => $options,
  );
  $form['#theme'] = 'game_calendar_settings_form';
  $form['game_calendars'] = array(
    '#type' => 'value',
    '#value' => $game_calendars,
  );
  $form['calendars'] = array(
    '#tree' => TRUE,
  );
  foreach ($game_calendars as $calendar => $state) {
    $form['calendars'][$calendar] = array();
    $form['calendars'][$calendar]['selection'] = array(
      '#type' => 'checkbox',
    );
    $form['calendars'][$calendar]['title'] = array(
      '#type' => 'item',
      '#value' => l(t('@title', array('@title' => $state->title)), 'admin/settings/game_calendar/'. $calendar),
    );
    $clock = game_clock_state($state->cid, TRUE);
    $form['calendars'][$calendar]['clock'] = array(
      '#type' => 'item',
      '#value' => t('!clock', array('!clock' => l(t('@clock', array('@clock' => $clock->title)), 'admin/settings/game_clock/'. $clock->name))),
    );
    $form['calendars'][$calendar]['state'] = array(
      '#type' => 'item',
      '#value' => t('@state', array('@state' => ($state->status ? t('started') : t('paused')))),
    );
    $form['calendars'][$calendar]['date'] = array(
      '#type' => 'item',
      '#value' => t('@date', array('@date' => date_format($state->date, 'Y-m-d H:i'))),
    );
    $form['calendars'][$calendar]['rate'] = array(
      '#type' => 'item',
      '#value' => t('@rate', array('@rate' => $state->rate)),
    );
    $ops = array(
      l(t('edit'), 'admin/settings/game_calendar/'. $calendar),
      l(t('delete'), 'admin/settings/game_calendar/'. $calendar .'/delete'),
    );
    $form['calendars'][$calendar]['ops'] = array(
      '#type' => 'item',
      '#value' => implode(', ', $ops),
    );
    $form['help'] = array(
      '#type' => 'item',
      '#value' => t('If a game calendar has been started, it will increment a turn every time the increment time passes. If %init is set to Yes, then the calendar will check for incrementation as appropriate on user page loads. Otherwise, it will check only during the cron run.', array('%init' => t('Page load'))),
    );
    $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  }
  return $form;
}

function game_calendar_list_form_submit($form, &$form_state) {
  $calendars = array();
  foreach ($form_state['values']['calendars'] as $calendar => $value) {
    if ($value['selection']) {
      $calendars[] = $calendar;
    }
  }
  if (!empty($calendars)) {
    switch ($form_state['values']['selector']) {
      case 'delete':
      case 'reset':
        $form_state['rebuild'] = TRUE;
        break;
      case 'pause':
        foreach ($calendars as $calendar) {
          game_calendar_pause($calendar);
          drupal_set_message(t("Paused the %calendar game calendar.", array('%calendar' => $form_state['values']['game_calendars'][$calendar]->title)));
        }
        break;
      case 'start':
        foreach ($calendars as $calendar) {
          game_calendar_start($calendar);
          drupal_set_message(t("Started the %calendar game calendar.", array('%calendar' => $form_state['values']['game_calendars'][$calendar]->title)));
        }
        break;
    }
  }
}

/**
 *  Delete confirmation form.
 */
function game_calendar_delete_form(&$form_state, $calendars) {
  if (is_object($calendars)) {
    $title = t('Are you sure you wish to delete the %calendar game calendar?', array('%calendar' => $calendars->title));
    $message = t('If you press %delete, then the %calendar game calendar will be deleted, and may not be recovered.', array('%delete' => t('Delete'), '%calendar' => $calendars->title));
    $calendars = array($calendars->name => $calendars);
  }
  else if (is_array($calendars) && !empty($calendars)) {
    $title = t('Are you sure you wish to delete these game calendars?');
    $message = t('If you press %delete, then the following game calendars will be deleted:', array('%delete' => t('Delete')));
    $items = array();
    foreach ($calendars as $state) {
      $items[] = t('%calendar', array('%calendar' => $state->title));
    }
    $message .= theme('item_list', $items);
    $message .= t('These game calendars may not be recovered.');
  }
  else {
    drupal_not_found();
  }
  $form['calendars'] = array(
    '#type' => 'value',
    '#value' => $calendars,
  );
  $form['#submit'][] = 'game_calendar_delete_form_submit';
  return confirm_form($form, $title, 'admin/settings/game_calendar', $message, t('Delete'));
}

/**
 *  Delete the submitted game calendar(s).
 */
function game_calendar_delete_form_submit($form, &$form_state) {
  foreach ($form_state['values']['calendars'] as $calendar => $state) {
    game_calendar_delete($calendar, TRUE);
  }
  $form_state['redirect'] = 'admin/settings/game_calendar';
}

/**
 *  Reset confirmation form.
 */
function game_calendar_reset_form(&$form_state, $calendars) {
  if (is_object($calendars)) {
    $title = t('Are you sure you wish to reset the %calendar game calendar?', array('%calendar' => $calendars->title));
    $message = t('If you press %reset, then the %calendar game calendar will be reset to 0 turns, which may not be undone.', array('%reset' => t('Reset'), '%calendar' => $calendars->title));
    $calendars = array($calendars->name => $calendars);
  }
  else if (is_array($calendars) && !empty($calendars)) {
    $title = t('Are you sure you wish to reset these game calendars?');
    $message = t('If you press %reset, then the following game calendars will be reset to 0 turns:', array('%reset' => t('Reset')));
    $items = array();
    foreach ($calendars as $state) {
      $items[] = t('%calendar', array('%calendar' => $state->title));
    }
    $message .= theme('item_list', $items);
    $message .= t('This action may not be undone.');
  }
  else {
    drupal_not_found();
  }
  $form['calendars'] = array(
    '#type' => 'value',
    '#value' => $calendars,
  );
  $form['#submit'][] = 'game_calendar_reset_form_submit';
  return confirm_form($form, $title, 'admin/settings/game_calendar', $message, t('Reset'));
}

/**
 *  Reset the submitted game calendar(s).
 */
function game_calendar_reset_form_submit($form, &$form_state) {
  foreach ($form_state['values']['calendars'] as $calendar => $state) {
    game_calendar_reset($calendar);
  }
  $form_state['redirect'] = 'admin/settings/game_calendar';
}

/**
 *  Callback for admin/settings/game_calendar/%game_calendar.
 *  This displays the edit game calendar form.
 */
function game_calendar_edit_page($calendar) {
  if (!$calendar->cid) {
    drupal_not_found();
  }
  drupal_set_title(t('Edit @calendar', array('@calendar' => $calendar->title)));
  return drupal_get_form('game_calendar_form', $calendar);
}

/**
 *  Callback for admin/settings/game_calendar/%game_calendar/pause.
 *  This pauses the game calendar.
 */
function game_calendar_pause_page($calendar) {
  game_calendar_pause($calendar->name);
  drupal_set_message(t("Paused the %calendar game calendar.", array('%calendar' => $calendar->title)));
  drupal_goto('admin/settings/game_calendar');
}

/**
 *  Callback for admin/settings/game_calendar/%game_calendar/start.
 *  This pauses the game calendar.
 */
function game_calendar_start_page($calendar) {
  game_calendar_start($calendar->name);
  drupal_set_message(t("Started the %calendar game calendar.", array('%calendar' => $calendar->title)));
  drupal_goto('admin/settings/game_calendar');
}

/**
 *  Define the form to create a new game calendar.
 */
function game_calendar_form($form_state, $calendar = NULL) {
  $calendar = (object) $calendar;
  $form = array();
  $form['calendar'] = array(
    '#type' => 'fieldset',
    '#title' => !isset($calendar->cid) ? t('Create new game calendar') : t('Game calendar'),
    '#collapsible' => !isset($calendar->cid) && (arg(3) != 'add'),
    '#collapsed' => !isset($calendar->cid) && (arg(3) != 'add'),
    '#description' => t('Game calendars can be useful for tracking game turns, special effects, and the like. You can also create calendars programmatically. See the !help page for more information.', array('!help' => l('game calendar help', 'admin/help/game_calendar'))),
  );
  $form['calendar']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#required' => TRUE,
    '#description' => t('A unique machine name for this game calendar. It must contain only lower-case, alphanumeric characters and underscores, and begin with a letter.'),
    '#default_value' => isset($calendar->name) ? $calendar->name : '',
  );
  $form['calendar']['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#description' => t('A human readable title for this game calendar.'),
    '#default_value' => isset($calendar->title) ? $calendar->title : '',
    '#required' => TRUE,
  );
  $form['calendar']['type'] = array(
    '#type' => 'textfield',
    '#title' => t('Type'),
    '#description' => t('The optional type of the game calendar. This is useful for sorting and filtering game calendars on the listing page. Additionally, some modules may act on or affect only calendars of this type.'),
    '#default_value' => isset($calendar->type) ? $calendar->type : '',
  );
  $form['calendar']['status'] = array(
    '#type' => 'checkbox',
    '#title' => t('Start calendar'),
    '#description' => t('If checked, then this game calendar will begin started. Otherwise, it will be paused.'),
    '#default_value' => isset($calendar->status) ? $calendar->status : TRUE,
  );
  $form['calendar']['turn'] = array(
    '#type' => 'textfield',
    '#title' => t('Starting turn'),
    '#default_value' => isset($calendar->turn) ? $calendar->turn : 0,
    '#description' => t('What turn should the calendar begin at?'),
  );
  $form['calendar']['increment'] = array(
    '#type' => 'select',
    '#title' => t('Increment'),
    '#options' => _game_calendar_selections(),
    '#default_value' => isset($calendar->increment) ? $calendar->increment : variable_get('game_calendar_increment_default', 5),
    '#required' => TRUE,
    '#description' => t('The calendar will wait at least this much time before incrementing to the next tick.'),
  );
  $form['calendar']['block'] = array(
    '#type' => 'checkbox',
    '#title' => t('Create display block'),
    '#default_value' => isset($calendar->block) ? $calendar->block : FALSE,
    '#description' => t('If checked, then make a block available that will display the current turn and status of this calendared.'),
  );
  $form['calendar']['init'] = array(
    '#type' => 'checkbox',
    '#title' => t('Increment calendar on page load'),
    '#default_value' => isset($calendar->init) ? $calendar->init : TRUE,
    '#description' => 'If checked, then this calendar will be checked for incrementation on normal page loads. Otherwise, it will only be checked during a cron run. If a calendar has an increment value higher than the cron settings, it may be useful to check this box.',
  );
  $form['calendar']['submit'] = array(
    '#type' => 'submit',
    '#value' => isset($calendar->cid) ? t('Save changes') : t('Create calendar'),
  );
  $form['calendar']['cid'] = array(
    '#type' => 'value',
    '#value' => $calendar->cid,
  );
  return $form;
}

/**
 *  Callback form for admin/settings/game_calendar/settings.
 */
function game_calendar_settings() {
  // This creates a list of the number of calendars to limit.
  $options = array(0 => 'All', 1 => 1, 5 => 5, 10 => 10, 25 => 25, 50 => 50, 100 => 100, 250 => 250, 500 => 500, 1000 => 1000);
  $form = array();
  $form['game_calendar_settings_form_count'] = array(
    '#type' => 'select',
    '#title' => t('Settings listing form calendar count'),
    '#description' => t('This displays how many calendars to display on the game calendar settings listing form page.'),
    '#options' => $options,
    '#default_value' => variable_get('game_calendar_settings_form_count', 50),
  );
  $form['game_calendar_increment_default'] = array(
    '#type' => 'select',
    '#title' => t('Increment default'),
    '#description' => t('This determines the default increment value for a newly created calendar. This value may easily be set when creating calendars, and is simply provided as a convenience for administrators and developers who need to create a large number of similar calendars.'),
    '#options' => _game_calendar_selections(),
    '#default_value' => variable_get('game_calendar_increment_default', 5),
  );
  $form['game_calendar_limit_on_init'] = array(
    '#type' => 'select',
    '#title' => t('Page load limit'),
    '#description' => t('This limits how many calendars to process during a single page load. Set it if you have a high volume of game calendars and have pages timing out.'),
    '#options' => $options,
    '#default_value' => variable_get('game_calendar_limit_on_init', 0),
  );
  $form['game_calendar_limit_on_cron'] = array(
    '#type' => 'select',
    '#title' => t('Cron limit'),
    '#description' => t('This limits how many calendars to process during each cron run. Set it if you have a high volume of game calendars that never seem to run.'),
    '#options' => $options,
    '#default_value' => variable_get('game_calendar_limit_on_cron', 0),
  );
  return system_settings_form($form);
}

/**
 *  Form validation for game_calendar_form.
 *  @see game_calendar_form
 */
function game_calendar_form_validate($form, &$form_state) {
  $state = $form_state['values'];
  // Ensure a safe machine name.
  if (!preg_match('/^[a-z_][a-z0-9_]*$/', $state['name'])) {
    form_set_error('name', t('The game calendar name may only contain lowercase letters, underscores, and numbers; and begin with a letter.'));
  }
  else {
    // Ensure the machine name is unique.
    $test_state = game_calendar_state($state['name']);
    if ($test_state->cid && ($test_state->cid != $state['cid'])) {
      form_set_error('name', t('Game calendar names must be unique. This game calendar name is already in use.'));
    }
  }
  if (isset($state['cid'])) {
    $test_state = game_calendar_state($state['cid'], TRUE);
    if (!isset($test_state->cid)) {
      form_set_error('', t('This game calendar has been deleted.'));
    }
  }
}

/**
 *  Form submission for game_calendar_form.
 *  @see game_calendar_form
 */
function game_calendar_form_submit($form, &$form_state) {
  if (isset($form_state['values']['cid'])) {
    // Get the currently stored state.
    $state = game_calendar_state($form_state['values']['cid'], TRUE);

    // Store the old name, so we can redirect if it's changed.
    $old_name = $state->name;

    // Only save if the state is already in the database.
    // This should already be taken care of in the validation step.
    if ($state->cid) {
      // By calling array_merge, we replace the $state with the new values.
      $state = array_merge((array) $state, $form_state['values']);

      // Save our new game calendar state.
      $state = game_calendar_save($state);

      drupal_set_message(t("Saved changes to the %calendar game calendar.", array('%calendar' => $state->title)));

      // If we changed the name, then redirect to the new edit page.
      if ($state && ($old_name != $state->name)) {
        drupal_goto('admin/settings/game_calendar/'. $state->name);
      }
    }
  }
  else {
    // Create a new game calendar state.
    $state = game_calendar_create($form_state['values'], TRUE);
  }
}

/**
 *  Build an array of increment times, suitable for a select drop-down list.
 *  @param $increment
 *    (optional) The number of seconds for which we wish to see a description.
 *  @return
 *    If $increment is NULL, then return the entire array.
 *    If it's a key in the array, then return the resulting description.
 *    Otherwise, return a translated string of '@increment seconds'.
 */
function _game_calendar_selections($increment = NULL) {
  static $selections;
  if (is_null($selections)) {
    $selections = array(
      0 => t('Never'),
      1 => t('1 seconds'),
      2 => t('2 seconds'),
      3 => t('3 seconds'),
      4 => t('4 seconds'),
      5 => t('5 seconds'),
      10 => t('10 seconds'),
      15 => t('15 seconds'),
      30 => t('30 seconds'),
      60 => t('1 minute'),
      120 => t('2 minutes'),
      180 => t('3 minutes'),
      240 => t('4 minutes'),
      300 => t('5 minutes'),
      600 => t('10 minutes'),
      900 => t('15 minutes'),
      1800 => t('30 minutes'),
      3600 => t('1 hour'),
      (3600 * 2) => t('2 hours'),
      (3600 * 3) => t('3 hours'),
      (3600 * 4) => t('4 hours'),
      (3600 * 5) => t('5 hours'),
      (3600 * 6) => t('6 hours'),
      (3600 * 8) => t('8 hours'),
      (3600 * 10) => t('10 hours'),
      (3600 * 12) => t('12 hours'),
      (3600 * 24) => t('1 day'),
      (3600 * 24 * 2) => t('2 days'),
      (3600 * 24 * 3) => t('3 days'),
      (3600 * 24 * 4) => t('4 days'),
      (3600 * 24 * 5) => t('5 days'),
      (3600 * 24 * 6) => t('6 days'),
      (3600 * 24 * 7) => t('7 days'),
    );
  }
  if (is_null($increment)) {
    return $selections;
  }
  if (isset($selections[$increment])) {
    return $selections[$increment];
  }
  return t('@increment seconds', array('@increment' => $increment));
}

/**
 *  Render the form as a table of calendar settings and ops.
 */
function theme_game_calendar_settings_form($form) {
  $output .= drupal_render($form['selector']);
  $header = $form['table_header']['#value'];
  $rows = array();
  foreach ($form['game_calendars']['#value'] as $calendar => $state) {
    $cells = array();
    $cells[] = drupal_render($form['calendars'][$calendar]['selection']);
    $cells[] = drupal_render($form['calendars'][$calendar]['title']);
    $cells[] = drupal_render($form['calendars'][$calendar]['clock']);
    $cells[] = drupal_render($form['calendars'][$calendar]['state']);
    $cells[] = drupal_render($form['calendars'][$calendar]['date']);
    $cells[] = drupal_render($form['calendars'][$calendar]['rate']);
    $cells[] = drupal_render($form['calendars'][$calendar]['ops']);
    $rows[] = $cells;
  }
  $output .= theme('table', $header, $rows);
  $output .= drupal_render($form);

  return $output;
}
