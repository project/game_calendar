<?php

/**
 *  @file
 *  Menu definition for the Game Calendar module.
 */

/**
 *  Helper function for game_calendar_menu().
 */
function _game_calendar_menu() {
  $items = array(
    'admin/settings/game_calendar' => array(
      'title' => 'Game calendars',
      'description' => 'Administer the game calendars.',
      'page callback' => 'game_calendar_list_page',
      'access arguments' => array('administer game calendar'),
      'file' => 'includes/game_calendar.admin.inc',
    ),
  );
  return $items;
}
